require 'rubygems'

require 'rails'

require 'rbconfig'
require 'rails/script_rails_loader'

require 'rails/ruby_version_check'

Signal.trap("INT") { puts; exit }

require 'rails/generators'
require 'rails/generators/rails/app/app_generator'

output = Rails::Generators::AppGenerator.start 'blah'

if output.is_a? Array
  puts "Hooray!"
else
  puts "Noooh!"
end
